
const express = require('express');
const path = require('path');
const app = express();
const mongoose = require('mongoose');
const Post = require('./models/post');
const Donate = require('./models/donate');

const methodOverride = require('method-override');
const postRoutes = require('./route/post-routes');

const createPath = require('./helpers/create-path');


app.set('view engine', 'ejs');


const bodyParser = require('body-parser');

const cors = require('cors');



app.use(express.urlencoded({ extended: false }));

const db = 'mongodb+srv://arzamaliev:Pass123@cluster0.ivrmxq6.mongodb.net/?retryWrites=true&w=majority'
mongoose
  .connect(db, { useNewUrlParser: true, useUnifiedTopology: true })
  .then((res) => console.log('Connected to DB'))
  .catch((error) => console.log(error));

app.use(cors());
// Настройка стат каталогов
app.use(express.static('src'));






app.use(methodOverride('_method'));

// app.use('/css', express.static(path.join(__dirname, 'src/css')));
// app.use('/img', express.static(path.join(__dirname, 'src/img')));
//app.use(express.static(path.join(__dirname, 'templates')));

// app.use('/data.json', express.static(path.join(__dirname, 'data.json')));
app.use(bodyParser.json());


app.get('/data.json', (req, res) => {
    res.sendFile(__dirname + '/data.json');
});



app.get('/', (req, res) => {
    res.render(createPath('index'));
});

app.get('/life', (req, res) => {
    res.render(createPath('life'));
});
app.get('/add-donate', (req, res) => {
    res.render(createPath('add-donate'));
});
app.get('/volonteer', (req, res) => {
    res.render(createPath('volonteer'));
});
app.get('/stats', (req, res) => {
    res.render(createPath('stats'));
});
app.get('/neur', (req, res) => {
    res.render(createPath('neur'));
});
app.get('/moscow', (req, res) => {
    res.render(createPath('moscow'));
});
// app.get('/posts', (req, res) => {
//     res.render(createPath('posts'));
// });
// app.get('/templates/index.html', (req, res) => {
//   res.sendFile(__dirname + '/templates/index.html')
//    })
// app.get('/templates/donate.html', (req, res) => {
//     res.sendFile(__dirname + '/templates/donate.html')
//    })   
// app.get('/templates/life.html', (req, res) => {
//     res.sendFile(__dirname + '/templates/life.html')
//    })
// app.get('/templates/moscow.html', (req, res) => {
//     res.sendFile(__dirname + '/templates/moscow.html')
//    })
// app.get('/templates/neur.html', (req, res) => {
//     res.sendFile(__dirname + '/templates/neur.html')
//    })
// app.get('/templates/stats.html', (req, res) => {
//     res.sendFile(__dirname + '/templates/stats.html')
//    })
// app.get('/templates/volonteer.html', (req, res) => {
//     res.sendFile(__dirname + '/templates/volonteer.html')
//    })


   
  
let port = 3001
app.listen(port, () => {
    console.log (`Сервер запущен: http://localhost:${port}`)
})
  
  
  app.use(postRoutes);




const donate = require('./models/donate');

app.get('/donates/:id', (req, res) => {
    const reason = 'Donate';
    Donate
      .findById(req.params.id)
      .then(donate => res.render(createPath('donate'), { donate, reason }))
      .catch((error) => {
        console.log(error);
        res.render(createPath('error'), { reason: 'Error' });
      });
  });


  app.delete('/donates/:id', (req, res) => {
    Donate
      .findByIdAndDelete(req.params.id)
      .then((result) => {
        res.sendStatus(200);
      })
      .catch((error) => {
        console.log(error);
        res.render(createPath('error'), { reason: 'Error' });
      });
  });


  app.get('/editd/:id', (req, res) => {
    const reason = 'Edit Donate';
    donate
      .findById(req.params.id)
      .then(donate => res.render(createPath('edit-donate'), { donate, reason }))
      .catch((error) => {
        console.log(error);
        res.render(createPath('error'), { reason: 'Error' });
      });
  });

  app.put('/editd/:id', (req, res) => {
    const { name, amount, reason  } = req.body;
    const { id } = req.params;
    donate
      .findByIdAndUpdate(id, { name, amount, reason  })
      .then((result) => res.redirect(`/donates/${id}`))
      .catch((error) => {
        console.log(error);
        res.render(createPath('error'), { reason: 'Error' });
      });
  });


  app.get('/donates', (req, res) => {
    const reason = 'Donates';
    donate
      .find()
      .sort({ createdAt: -1 })
      .then(donates => res.render(createPath('donates'), { donates, reason }))
      .catch((error) => {
        console.log(error);
        res.render(createPath('error'), { reason: 'Error' });
      });
  });
  
  app.get('/add-donate', (req, res) => {
    const reason = 'Add Post';
    res.render(createPath('add-donate'), { reason });
  });
  
  app.post('/add-donate', (req, res) => {
    const { name, amount, reason } = req.body;
    const donate = new Donate({ name, amount, reason });
    donate
      .save()
      .then((result) => res.redirect('/donates'))
      .catch((error) => {
        console.log(error);
        res.render(createPath('error'), { reason: 'Error' });
      });
  });