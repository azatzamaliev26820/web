const path = require('path');

const createPath = (page) => path.resolve(__dirname, '../ejs-templates', `${page}.ejs`);

module.exports = createPath;