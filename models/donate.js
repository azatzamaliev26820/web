// donation.js

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const donateSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  amount: {
    type: String,
    required: true,
  },
  reason: {
    type: String,
    required: true,
  }
}, { timestamps: true });

const Donate = mongoose.model('Donate', donateSchema);
module.exports = Donate;
